# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import gi
import lxml.etree as ET
import os

gi.require_version("Gtk", "3.0")
gi.require_version('GtkSource', '4')
from gi.repository import GLib, Gio, Gtk, Gdk, GtkSource, GObject

import utils
import widgets
from utils import debug


@Gtk.Template(filename="ui/console_page.ui")
class ConsolePage(Gtk.ScrolledWindow):
    __gtype_name__ = "ConsolePage"

    srcview = Gtk.Template.Child()
    txtbuffer = Gtk.Template.Child()

    def __init__(self, path, *args, **kwargs):
        GObject.type_register(GtkSource.View)
        GObject.type_register(GtkSource.Buffer)
        super().__init__(*args, **kwargs)
        self.srcview.set_can_focus(True)
        self.srcview.set_focus_on_click(True)
        self.srcview.set_left_margin(30)
        self.srcview.set_name("srcview")  # to change style inc. font-size
        self.srcview_gcp = Gtk.CssProvider()
        self.srcview.get_style_context().add_provider(self.srcview_gcp, Gtk.STYLE_PROVIDER_PRIORITY_USER)
        lm = GtkSource.LanguageManager()
        self.txtbuffer.set_language(lm.get_language("python"))
        self.path = path
        self.srcview.get_space_drawer().set_enable_matrix(True)
        self.show_all()

    def set_font_size(self, size):
        self.srcview_gcp.load_from_data(b"#srcview {font-size: %dpt}" % size)

    def set_css(self, css):
        self.srcview_gcp.load_from_data(b"#srcview " + css.encode("utf-8"))

    def set_text(self, text):
        self.txtbuffer.set_text(text)
        self.txtbuffer.place_cursor(self.txtbuffer.get_start_iter())


@Gtk.Template(filename="ui/console.ui")
class Console(Gtk.Window):
    __gtype_name__ = "Console"
    __gsignals__ = {"run-activate": (GObject.SignalFlags.ACTION, None, ()), }

    nb = Gtk.Template.Child()
    snippets = Gtk.Template.Child()
    sncell = Gtk.Template.Child()
    osd = Gtk.Template.Child()
    osd_entry = Gtk.Template.Child()
    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app
        self.connect("delete-event", self.on_destroy)
        self.connect("run-activate", self.on_run_activate)
        target_entries_dst = [('text/plain', 0, 0), ("GTK_TREE_MODEL_ROW", 0, 0)]
        self.snippets.enable_model_drag_dest(target_entries_dst, Gdk.DragAction.COPY)
        self.snippets.connect('drag-data-received', self.on_drag_data_received)
        self.snippets.connect("row-activated", self.on_snippet_activated)
        self.snippets.connect("key-press-event", self.on_snippet_keypress)
        self.sncell.connect("edited", self.on_sncell_edited)
        self.osd_entry.connect("key-press-event", self.on_osd_entry_keypress)
        self.osd.connect("closed", self.on_osd_closed)
        self.console_scheme = None
        self.console_fontname = None
        self.console_fontsize = 24
        self.setup()
        self.show_all()

    @Gtk.Template.Callback()
    def on_menu_activate(self, w):
        action = w.get_label()
        actions = { "New": self.on_menu_new,
                    "Open":self.on_menu_open,
                    "Save": self.on_menu_save,
                    "Save As": self.on_menu_save_as,
                    "Close": self.on_menu_close,
                    "Change Font": self.on_menu_change_font,
                    "Change Theme": self.on_menu_change_theme
                    }
        if action in actions:
            actions[action]()

    def on_menu_close(self):
        pass


    def font_filter(self, pffam, pffac):
        pfd = pffac.describe()
        res = True
        if not pffam.is_monospace():
            res = False
        if pfd.get_style() != 0:
            res = False
        if pfd.get_weight() not in range(300, 501):
            res = False
        if pfd.get_stretch() not in range(3, 6):
            res = False
        fn = pffam.get_name().lower()
        if "emoji" in fn or "icon" in fn or "mathjax" in fn:
            res = False
        return res

    def on_menu_change_font(self):
        dialog = Gtk.FontChooserDialog(
            title="Please choose a font", parent=self
        )
        dialog.set_filter_func(self.font_filter)
        dialog.set_font("%s %s" % (self.console_fontname, self.console_fontsize))
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.console_fontsize = dialog.get_font_size()/1024
            self.console_fontname = dialog.get_font_family().get_name()
            css = "{font-family: %s; font-size: %dpt}" % (self.console_fontname, self.console_fontsize)
            for x in range(self.nb.get_n_pages()):
                self.nb.get_nth_page(x).set_css(css)
        dialog.destroy()

    def on_theme_selected(self, tv, tp, tvc, dlg):
        mdl = tv.get_model()
        iter1 = mdl.get_iter(tp)
        tid = mdl.get_value(iter1, 0)
        dlg.destroy()
        self.console_scheme = GtkSource.StyleSchemeManager.get_default().get_scheme(tid)
        for x in range(self.nb.get_n_pages()):
            self.nb.get_nth_page(x).txtbuffer.set_style_scheme(self.console_scheme)


    def on_menu_change_theme(self):
        page = self.nb.get_nth_page(0)
        if not page:
            return
        tid = page.txtbuffer.get_style_scheme().get_id()
        themes = GtkSource.StyleSchemeManager.get_default().get_scheme_ids()
        tidpath = themes.index(tid)
        tv = Gtk.TreeView()
        c = Gtk.CellRendererText()
        c0 = Gtk.TreeViewColumn('', c, text=0)
        tv.append_column(c0)
        tv.set_headers_visible(False)
        mdl = Gtk.ListStore(GObject.TYPE_STRING)
        tv.set_model(mdl)
        dlg = Gtk.Dialog("Select Theme", parent=self)
        tv.connect("row-activated", self.on_theme_selected, dlg)
        for t in themes:
            mdl.append([t])
        tv.set_cursor(tidpath, None, False)
        tv.show_all()
        dlg.get_content_area().pack_start(tv, 1, 1, 6)
        dlg.run()


    def on_osd_closed(self, *args, **kwargs):
        self.add_snippet_data = None

    def on_osd_entry_keypress(self, w, e):
        w.grab_remove()
        kn = Gdk.keyval_name(e.keyval)
        if kn == "Return":
            name = w.get_text()
            if not name:
                self.osd.popdown()
                return
            widget, drag_context, x, y, data = self.add_snippet_data
            self.osd.popdown()
            path, pos = None, None
            dst = widget.get_dest_row_at_pos(x, y)
            txt = data.get_text()
            txt = txt.replace('&', '&amp;')
            txt = txt.replace('<', '&lt;')
            txt = txt.replace('>', '&gt;')
            model = widget.get_model()
            if dst:
                path, pos = dst
                path = model.get_iter(path)
            if pos == Gtk.TreeViewDropPosition.BEFORE:
                model.insert_before(None, model.get_iter(path), [name, txt])
            elif pos == Gtk.TreeViewDropPosition.AFTER:
                model.insert_after(None, path, [name, txt])
            else:
                model.append(path, [name, txt])
            self.update_snippets()

    def on_sncell_edited(self, cell, path, newtext):
        if not newtext:
            return
        model = self.snippets.get_model()
        niter = model.get_iter(path)
        name = model.get_value(niter, 0)
        if name == newtext:
            return
        model.set(niter, 0, newtext)
        # TODO: save snippets change?

    def on_snippet_keypress(self, widget, e):
        kn = Gdk.keyval_name(e.keyval)
        selection = widget.get_selection()
        model, itr = selection.get_selected()
        if not itr:
            return
        itr_path = model.get_path(itr)
        if kn == "Delete":
            # TODO: Confirmation to delete?
            model.remove(itr)
            if model.get_iter_first():
                if itr_path:
                    widget.set_cursor(itr_path)
                    widget.grab_focus()
            self.update_snippets()

    def on_snippet_reordered(self, *args, **kwargs):
        print("OSR", args, kwargs)

    def on_snippet_activated(self, view, path, column):
        mdl = self.snippets.get_model()
        iter1 = mdl.get_iter(path)
        txt = mdl.get_value(iter1, 1)
        txt = txt.replace('&amp;', '&')
        txt = txt.replace('&lt;', '<')
        txt = txt.replace('&gt;', '>')
        pn = self.nb.get_current_page()
        if pn != -1:
            tb = self.nb.get_nth_page(pn).txtbuffer
            tb.insert_at_cursor(txt)

    def on_drag_data_received(self, widget, drag_context, x, y, data, info, time):
        if data.get_target().name() == "text/plain":
            self.add_snippet_data = (widget, drag_context, x, y, data)
            r = self.snippets.get_cell_area(1, None)
            r.y = y
            r.x = x
            self.osd.set_relative_to(self.snippets)
            self.osd.set_pointing_to(r)
            self.osd.show_all()
            self.osd.popup()

    def add_script(self, name, text, path=None):
        new_page = ConsolePage(path=path)
        tablabel = widgets.TabLabel(label=name)
        tablabel.close_btn.connect("clicked", self.on_close_tab, new_page)
        pn = self.nb.append_page(new_page, tablabel)
        self.nb.set_menu_label_text(new_page, name)
        if pn > -1:
            self.nb.set_current_page(pn)
            if text:
                new_page.set_text(text)
        new_page.srcview.grab_focus()
        if self.console_scheme:
            new_page.txtbuffer.set_style_scheme(self.console_scheme)
        if self.console_fontname:
            css = "{font-family: %s; font-size: %dpt}" % (self.console_fontname, self.console_fontsize)
            new_page.set_css(css)

    def on_close_tab(self, tablabel, page):
        pn = self.nb.page_num(page)
        if pn > -1:
            self.nb.remove_page(pn)

    def setup(self):
        cfgpath = os.path.join(self.app.basepath, "cli_config.xml")
        scrpath = os.path.join(self.app.basepath, "scripts")
        os.makedirs(scrpath, exist_ok=True)
        if not os.path.exists(cfgpath):
            self.add_script("New 1", "")
            return
        try:
            tree = ET.parse(cfgpath)
            root = tree.getroot()
            for scr in  root.findall("script"):
                fpath = os.path.join(cfgpath, scr.attrib["path"])
                if os.path.exists(fpath):
                    with open(fpath, "r") as f:
                        txt = f.read()
                        self.add_script(scr.attrib["name"], txt, fpath)
        except Exception as e:
            print("Failed to parse configuration file! (%s)" % e)
        snippath = os.path.join(self.app.basepath, "cli_snippets.dmp")
        model = self.snippets.get_model()
        try:
            with open(snippath, "r") as f:
                lines = f.readlines()
                pos = 0
                while pos < len(lines):
                    name = lines[pos][:-1]
                    pos += 1
                    lns = lines[pos][:-1]
                    pos += 1
                    lns = int(lns)
                    txt = "".join(lines[pos:pos + lns])[:-1]
                    pos += lns
                    # FIXME! I use tree not list
                    model.append(None, [name, txt])
        except:
            pass

    def on_destroy(self, *args):
        del self.app.windows["console"]

    def on_menu_new(self):
        name = "New %s" % (self.nb.get_n_pages() + 1)
        self.add_script(name, "")

    def on_menu_open(self):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=self,
             action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
        )
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            files = [dialog.get_filename()]
            for fpath in files:
                txt = ""
                with open(fpath, "r") as f:
                    txt = f.read()
                name = os.path.split(fpath)[1]
                self.add_script(name, txt, fpath)
        dialog.destroy()

    def on_run_activate(self, *args):
        pn = self.nb.get_current_page()
        if pn > -1:
            page = self.nb.get_nth_page(pn)
            exec(page.txtbuffer.get_property("text"), globals(), {"self":self.app})

    def update_snippets(self):
        cfgpath = os.path.join(self.app.basepath, "cli_snippets.dmp")
        with open(cfgpath, "w") as f:
            mdl = self.snippets.get_model()
            for name, text in mdl:
                f.write(name + "\n")
                # FIXME! I use tree not list; need to store path
                f.write("%d\n" % (text.count("\n") + 1))
                f.write(text + "\n")

    def update_config(self):
        cfgpath = os.path.join(self.app.basepath, "cli_config.xml")
        root = ET.Element(self.app.appname)
        for x in range(self.nb.get_n_pages()):
            page = self.nb.get_nth_page(x)
            name = self.nb.get_tab_label(page).label.get_text()
            if not page.path:
                # need to save first
                continue
            scr = ET.SubElement(root, "script")
            scr.set("name", name)
            scr.set("path", page.path)
        utils.indent(root)
        tree = ET.ElementTree(root)
        tree.write(cfgpath)

    def on_menu_save_as(self):
        self.on_menu_save(True)

    def on_menu_save(self, with_shift=None):
        pn = self.nb.get_current_page()
        if pn == -1:
            return
        page = self.nb.get_nth_page(pn)
        if with_shift or page.path is None:
            # Save As
            dialog = Gtk.FileChooserDialog(
                title="Please choose a file", parent=self,
                 action=Gtk.FileChooserAction.SAVE
            )
            dialog.add_buttons(
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
            )
            lbl = self.nb.get_tab_label(page).label
            lbltext = lbl.get_text()
            dialog.set_file(Gio.File.new_for_path(os.path.join(self.app.basepath, "scripts", lbltext)))
            path = None
            response = dialog.run()
            if response == Gtk.ResponseType.OK:
                path = dialog.get_filename()
            dialog.destroy()
            if path is None:
                return
            page.path = path
            name = os.path.split(path)[1]
            lbl.set_text(name)
            self.nb.set_menu_label_text(page, name)

        with open(page.path, "w") as f:
            f.write(page.txtbuffer.get_property("text"))
        # TODO: need to drop "unsaved changes" state for the tab
        self.update_config()


    def _get_hv(self):
        return self.app.windows["main"].main_nb.get_nth_page(0).get_child().get_child().get_children()[1].get_children()[1]
