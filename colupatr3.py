#!/usr/bin/env python3
#
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

'''
colupatre in py3/gi
'''

import datetime
import os
import re
import struct
import sys
import time
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gio, Gtk, Gdk, GObject

from console import Console
import formats.utils
import gui
import hv3
import utils
import widgets
from utils import debug

APPNAME = "colupatr3"
VERSION = "0.8.3"

class AppPage(Gtk.ScrolledWindow):
    __gtype_name__ = "AppPage"

    def __init__(self, path, app, *args, **kwargs):
        super().__init__(*args)
        self.path = path
        self.app = app
        self.kwargs = kwargs
        self.search_tables = {}
        self.load_file(kwargs.get("data"))
        self.show_all()

    def load_file(self, buf=None):
        if buf is None:
            with open(self.path, "rb") as f:
                buf = f.read()
        self.load_rlp(buf)
        self.add(self.hexview)

    def rlp_unpack(self, buf, off):
        dl = buf[off]
        off += 1
        k = buf[off:off + dl].decode("utf-8")
        off += dl
        fmt = buf[off:off + 1]
        off += 1
        if fmt in b"<>":
            fmt += buf[off:off + 1]
            off += 1
            fmt.decode("utf-8")
        if fmt in b"sS*":
            vl = struct.unpack("<I", buf[off:off+4])[0]
            off += 4
            v = buf[off:off+vl].decode("utf-8")
            off += vl
        else:
            v = struct.unpack(fmt, buf[off:off + struct.calcsize(fmt)])[0]
            off += struct.calcsize(fmt)
        return off, k, v

    def rlp_pack(self, dscr, fmt, value, f):
        dl = len(dscr)
        f.write(struct.pack("B", dl))
        f.write(bytes(dscr, "utf-8"))
        f.write(bytes(fmt, "utf-8"))
        if fmt in "sS":
            vl = len(value)
            f.write(struct.pack("<I", vl))
            v = bytes(value, "utf-8")
        elif fmt == "*":
            v = struct.pack("<I", value)
        else:
            v = struct.pack(fmt, value)
        f.write(v)

    def load_rlp(self, buf):
        if buf[:9] == b"RE-LABv08":
            offset = 35
            k = ""
            while k != "Num of lines":
                offset, k, v = self.rlp_unpack(buf, offset)

            lines = []
            for _ in range(v):
                off = struct.unpack("<I", buf[offset: offset + 4])[0]
                line = {"off":off}
                offset += 4
                flag = buf[offset]
                offset += 1
                if flag & 0x80:
                    # need to read clr and width for the mode
                    clr = [x/255. for x in struct.unpack("BBB", buf[offset: offset + 3])]
                    offset += 3
                    w = 1 + (flag & 7)
                    line["mode"] = clr, w
                if flag & 0x40:
                    clr = [x/255. for x in struct.unpack("BBB", buf[offset: offset + 3])]
                    offset += 3
                    clen = buf[offset]
                    offset += 1
                    txt = buf[offset:offset+clen].decode("utf-8")
                    offset += clen
                    line["nb"] = clr, txt
                shift = (flag & 0x38) >> 3
                if shift:
                    line["shift"] = shift
                lines.append(line)
            # need to skip "[x]Data BLOB*<dword>

            self.hexview = hv3.HexView(self, self.app.settings, buf[offset + 15:], lines=lines)
        else:
            self.hexview = hv3.HexView(self, self.app.settings, buf)


    def save(self, fname):
        with open(fname, "wb") as f:
            f.write(b"RE-LABv08 [DL(B)|D|VF(2c)|VL(<I)|V]")
            self.rlp_pack("Colupatr Version", "s", VERSION, f)
            self.rlp_pack("Change UID", "s", os.environ.get("USERNAME"), f)
            self.rlp_pack("Change time", "s", str(datetime.datetime.now().strftime("%Y-%b-%d %H:%M:%S")), f)
            self.rlp_pack("Line Format", "s", "[OFF(<I)|MODE_F(x)CMNT_F(x)SHIFT(xxx)W(xxx)|{MODE(RGB)}|{CMNT([RGB|LEN|TXT])}]", f)
            self.rlp_pack("Num of lines", "<I", len(self.hexview.lines), f)
            for l in self.hexview.lines:
                f.write(struct.pack("<I", l["off"]))
                flag = 0
                if "shift" in l:
                    flag += (l["shift"] & 7) << 3
                if "nb" in l:
                    flag += 0x40
                if "mode" in l:
                    c, w = l["mode"]
                    flag += 0x80 + ((w - 1) & 7)
                    mclr = (int(255 * c[2]) << 24) + (int(255 * c[1]) << 16) + (int(255 * c[0]) << 8) + flag
                    f.write(struct.pack("<I", mclr))
                    print("F80 %x" % flag)
                else:
                    f.write(struct.pack("B", flag))
                if "nb" in l:
                    c, t = l["nb"]
                    clr = (len(t) << 24) + (int(255 * c[2]) << 16) + (int(255 * c[1]) << 8) + int(255 * c[0])
                    f.write(struct.pack("<I", clr))
                    f.write(bytes(t, "utf-8"))

            self.rlp_pack("Data BLOB", "*", len(self.hexview.data), f)
            f.write(self.hexview.data)


    def html_export(self, fname, start_line=None, end_line=None, options=None):
        if not fname:
            return
        if not options:
            options = {}

        with open(fname, 'w') as f:
            f.write("<!DOCTYPE html><html><body>")
            f.write("<head>\n<meta charset='utf-8'>\n")
            f.write("<style type='text/css'>\ntr.top th { text-align: left; padding-left: 5px; border-bottom: 1px solid black;}")
            f.write("tr td {border-top: inherit;}\n")
            f.write(".mid { border-left: 1px solid black; border-right: 1px solid black; padding-left: 4px; padding-right: %dpx}\n" % self.hexview.max_shift)
            f.write(".asc { border-right: 1px solid gray; padding-left: 4px; padding-right: %dpx}\n" % self.hexview.max_shift)
            f.write(".cmnt {padding-left:4px}\n")
            f.write("</style>\n</head>\n")

            f.write("<table style='font-family:%s;' cellspacing=0>\n" % (options["font"] if "font" in options else "Monospace"))

            if "hdr" in options and options["hdr"] == "on":
                f.write("<tr class='top'><th></th><th style='border-left: 1px solid black; border-right: 1px solid black;'>%s</th><th></th><th></th></tr>" % utils.d2hex(range(self.hexview.hexlen), " "))

            for i, l in enumerate(self.hexview.lines[start_line: end_line], start_line):
                off = l["off"]
                addr = "%08x" % off
                ln_len = self.hexview.get_line_len(i)
                thex = utils.d2hex(self.hexview.data[off: off + ln_len], " ")
                tasc = utils.d2asc(self.hexview.data[off: off + ln_len])
                tcmn, tclr, shift = "", "", ""
                if "nb" in l:
                    clr, tcmn = l["nb"]
                    shift = " style='color:%s;" % Gdk.RGBA(*clr).to_string()
                    tclr = shift + "'"
                if "shift" in l:
                    if shift:
                        shift += "padding-left:%dpx;'" % (l["shift"] * self.hexview.max_shift / 4)
                    else:
                        shift = " style='padding-left:%dpx;'" % (l["shift"] * self.hexview.max_shift / 4)
                else:
                    shift += "'"
                brd = ""
                if "mode" in l:
                    clr, w = l["mode"]
                    brd = " style='border-top: %dpx solid %s;'" % (w, Gdk.RGBA(*clr).to_string())
                f.write("<tr%s><td>%s</td><td class='mid'%s>%s</td><td class='asc'>%s</td><td class='cmnt'%s>%s</td>" % (brd, addr, shift, thex, tasc, tclr, tcmn))
                f.write("</tr>\n")
            f.write("</table></body></html>")


class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id="re-lab.colupatr3",
            flags=Gio.ApplicationFlags.HANDLES_OPEN,
            **kwargs
        )
        self.appname = APPNAME
        self.execpath = os.path.dirname(os.path.abspath(__file__))
        self.windows = {}
        self.connect("open", self.open_files)

    def do_startup(self):
        Gtk.Application.do_startup(self)

        actions = {
            "open": self.on_open,
            "save": self.on_save,
            "export": self.on_export,
            "close": self.on_close,
            "console": self.on_console,
            "about": self.on_about,
            "manual": self.on_manual,
            "quit": self.on_quit
        }
        self.config()

        for k,v in actions.items():
            action = Gio.SimpleAction.new(k, None)
            action.connect("activate", v)
            self.add_action(action)

        action = Gio.SimpleAction.new("paste", None)
        action.connect("activate", self.on_paste)
        self.add_action(action)
        self.set_accels_for_action("app.paste", ["<Control>V"])

        self.builder = Gtk.Builder.new_from_file("ui/clptr3_app.ui")
        self.set_menubar(self.builder.get_object("menubar"))
        self.activate()

    def config(self):
        # default settings
        c1, c2 = Gdk.RGBA(), Gdk.RGBA()
        c1.parse("#4CFF4C")
        c2.parse("#4C4CFF")
        self.settings = {
            "hexview/fontsize": {"type": "int", "value": 20, "name": "Hexview/Font size"},
            "hexview/cur_clr": {"type": "rgb", "value": c1, "name": "Hexview/Positive selection color"},
            "hexview/cur_clr2": {"type": "rgb", "value": c2, "name": "Hexview/Negative selection color"},
            "hexview/hexlen": {"type": "int", "value": 16, "name": "Hexview/Line length"},
            "hexview/scrolling_speed": {"type": "float", "value": 2, "name": "Hexview/Scrolling speed"},
            "hexview/line_interval": {"type": "float", "value": 1.4, "name": "Hexview/Line interval"},
            "console/fontsize": {"type": "int", "value": 20, "name": "Console/Font size"},
            "statusbar/show_le": {"type": "bool", "value": True, "name": "Statusbar/Show LE value"},
            "statusbar/show_be": {"type": "bool", "value": True, "name": "Statusbar/Show BE value"},
            "statusbar/show_midi": {"type": "bool", "value": False, "name": "Statusbar/Show MIDI value"},
            "statusbar/show_color": {"type": "bool", "value": True, "name": "Statusbar/Show color"},
            "statusbar/div_only": {"type": "bool", "value": False, "name": "Statusbar/Show divided values only"},
#            "statusbar/divs": {"type": "list:float", "value": [], "name": "Statusbar/List of divisors"},
            "statusbar/divs": {"type": "txt", "value": "", "name": "Statusbar/List of divisors"},
            "statusbar/show_txt": {"type": "bool", "value": False, "name": "Statusbar/Show text"},
            "statusbar/show_ipv4": {"type": "bool", "value": False, "name": "Statusbar/Show IPv4"},
            "statusbar/encoding": {"type": "txt", "value": "utf-8", "name": "Statusbar/Text encoding"},
        }
        self.basepath = os.path.join(GLib.get_user_config_dir(), self.appname)
        self.cfgpath = os.path.join(self.basepath, "config.xml")
        self.execpath = os.path.dirname(os.path.abspath(__file__))
        utils.read_config(self.cfgpath, self.settings, self.basepath)
        if not os.path.exists(self.basepath):
            os.makedirs(self.basepath)

    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if "main" not in self.windows:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self.windows["main"] = gui.AppWindow(AppPage, application=self, title=self.appname)
        self.windows["main"].present()

    def on_paste(self, *args, **kwargs):
        clp = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        clp.request_text(self.on_clp_get_text)

    def on_clp_get_text(self, clp, text):
        txtlist = text.split()
        data = b""
        try:
            for i in txtlist:
                data += struct.pack("B", int(i, 16))
            self.windows["main"].add_file(path="Clipboard", data=data)
        except:
            print ("Not a copy of hexdump")

    def open_files(self, app, files, nfiles, hint):
        for f in files:
            self.windows["main"].add_file(f.get_path())
        self.windows["main"].present()
        return 0

    def on_close(self, action, param):
        pn = self.windows["main"].main_nb.get_current_page()
        if pn > -1:
            self.windows["main"].main_nb.remove_page(pn)

    def on_open(self, action, param):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=self.windows["main"], action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
        )
        response = dialog.run()
        files = [dialog.get_filename()]
        dialog.destroy()
        if response == Gtk.ResponseType.OK:
            for fpath in files:
                self.windows["main"].add_file(path=fpath)

    def on_export(self, action, param):
        pn = self.windows["main"].main_nb.get_current_page()
        if pn < 0:
            return
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=self.windows["main"], action=Gtk.FileChooserAction.SAVE
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_SAVE, Gtk.ResponseType.OK,
        )
        response = dialog.run()
        filename = dialog.get_filename()
        dialog.destroy()
        if response == Gtk.ResponseType.OK:
            page = self.windows["main"].main_nb.get_nth_page(pn)
            hv = page.hexview
            sln, eln = None, None
            if hv.cur_len:
                sln = hv.find_line(hv.cur_pos)
                eln = hv.find_line(hv.cur_pos + hv.cur_len)
                if hv.cur_len < 0:
                    sln, eln = eln, sln
            page.html_export(filename, sln, eln)

    def on_save(self, action, param):
        pn = self.windows["main"].main_nb.get_current_page()
        if pn < 0:
            return
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=self.windows["main"], action=Gtk.FileChooserAction.SAVE
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_SAVE, Gtk.ResponseType.OK,
        )
        response = dialog.run()
        filename = dialog.get_filename()
        dialog.destroy()
        if response == Gtk.ResponseType.OK:
            page = self.windows["main"].main_nb.get_nth_page(pn)
            page.save(filename)

    def on_console(self, action, param):
        if "console" not in self.windows:
            self.windows["console"] = Console(self)
        self.windows["console"].present()

    def on_manual(self, action, param):
        with open(os.path.join(self.execpath, 'docs/manual.txt'), "r") as f:
            help_text = f.read()

        label = Gtk.TextView()
        tb = label.get_buffer()
        si = tb.get_start_iter()
        tb.insert_markup(si, help_text, -1)
        label.set_editable(False)
        d = Gtk.Window()
        d.set_title("colupatr3: manual")
        sw = Gtk.ScrolledWindow()
        sw.add(label)
        sw.set_margin_start(24)
        d.set_size_request(1200, 800)
        d.add(sw)
        d.show_all()

    def on_about(self, action, param):
        builder = Gtk.Builder.new_from_file("ui/clptr3_about.ui")
        about_dialog = builder.get_object("about")
        about_dialog.set_transient_for(self.windows["main"])
        about_dialog.present()

    def on_quit(self, action, param):
        self.quit()


if __name__ == "__main__":
    try:
        app = Application()
        app.run(sys.argv)
    except KeyboardInterrupt:
        app.quit()

# END
